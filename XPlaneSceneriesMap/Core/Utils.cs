﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace XPlaneSceneriesMap.Core
{
	internal static class Utils
	{
		internal static bool StringsAreEqual(
			string stringA,
			string stringB
			)
			=> (
				0 == string.Compare(
					stringA,
					stringB,
					StringComparison.OrdinalIgnoreCase
					)
				);

		internal static string NormalizeString(string someString)
		{
			var retVal = new StringBuilder();

			foreach (var currentChar in someString)
			{
				if ('\'' == currentChar)
				{
					retVal.Append("\\'");
				}
				else
				{
					retVal.Append(currentChar);
				}
			}

			return retVal.ToString();
		}

		internal static string ReadStringUntilSpace(
			string someString,
			int startIdx
			)
		{
			var retVal = new StringBuilder();

			for (var i = startIdx + 1; i < someString.Length; i++)
			{
				if (' ' == someString[i])
				{
					break;
				}

				retVal.Append(someString[i]);
			}

			return retVal.ToString();
		}

		internal static string CalculateTopFoldersHash(string folderPath)
		{
			var subfolderPaths = Directory.GetDirectories(
				folderPath,
				"*.*",
				SearchOption.TopDirectoryOnly
				);

			string retVal;

			using (var md5 = MD5.Create())
			{
				for (var i = 0; i < subfolderPaths.Length; i++)
				{
					var folderNameBytes = Encoding.UTF8.GetBytes(
						subfolderPaths[i]
						);

					var weHaveLastFolderInTheList = (
						subfolderPaths.Length - 1 == i
						);

					if (!weHaveLastFolderInTheList)
					{
						md5.TransformBlock(
							folderNameBytes,
							0,
							folderNameBytes.Length,
							folderNameBytes,
							0
							);
					}
					else
					{
						md5.TransformFinalBlock(
							folderNameBytes,
							0,
							folderNameBytes.Length
							);
					}
				}

				retVal = BitConverter.ToString(md5.Hash)
					.Replace("-", string.Empty)
					.ToLowerInvariant();
			}

			return retVal;
		}
	}
}
