﻿namespace XPlaneSceneriesMap.Core.Enums
{
	internal enum SceneriesMapStatus
	{
		Unknown = 0,
		NotAvailable = 1,
		Outdated = 2,
		Actual = 4
	}
}
