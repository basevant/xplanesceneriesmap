﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using XPlaneSceneriesMap.Configuration;
using XPlaneSceneriesMap.Core.Enums;
using XPlaneSceneriesMap.Models;

namespace XPlaneSceneriesMap.Core
{
	internal static class Worker
	{
		#region Progress reporting

		internal delegate void MapSceneryHandler(
			int numberOfSceneries,
			int currentSceneryIdx
			);

		internal static event MapSceneryHandler OnMapSceneryProcess;

		#endregion


		private const string SceneryMarkersFilePath
			= @"html\scripts\scenery-markers.js";

		private const string SceneryMarkersPagePath
			= @"html\SceneriesMap.html";

		private static string GetSceneryMarkersFileFullPath()
			=> Path.Combine(
				AppDomain.CurrentDomain.BaseDirectory,
				SceneryMarkersFilePath
			);

		internal static string GetSceneryMarkersPageFullPath()
			=> Path.Combine(
				AppDomain.CurrentDomain.BaseDirectory,
				SceneryMarkersPagePath
			);

		internal static SceneryMapStatusModel GetSceneryMapStatus(
			string customSceneryFolderLocation
			)
		{
			var sceneryMarkersFullPath = GetSceneryMarkersFileFullPath();

			//	does scenery markers file exist?
			//

			if (
				!File.Exists(sceneryMarkersFullPath)
				|| !File.Exists(GetSceneryMarkersPageFullPath())
				)
			{
				return new SceneryMapStatusModel
				{
					MapStatus = SceneriesMapStatus.NotAvailable
				};
			}

			//	ok, let's check markers file actuality
			//

			if (!Directory.Exists(customSceneryFolderLocation))
			{
				return new SceneryMapStatusModel
				{
					MapStatus = SceneriesMapStatus.Unknown
				};
			}

			var markersFileLastEditDateTime = File.GetLastWriteTime(
				sceneryMarkersFullPath
				);

			var customSceneryFolderLastEditDateTime = Directory.GetLastWriteTime(
				customSceneryFolderLocation
				);

			var markersFileIsActual = Utils.StringsAreEqual(
				Configurator.CustomSceneryFolderHash,
				Utils.CalculateTopFoldersHash(
					customSceneryFolderLocation
					)
				);

			return new SceneryMapStatusModel
			{
				MarkersFileLastEditTimestamp
					= markersFileLastEditDateTime,

				CustomSceneryFolderLastEditTimestamp
					= customSceneryFolderLastEditDateTime,

				MapStatus = (
					markersFileIsActual
					? SceneriesMapStatus.Actual
					: SceneriesMapStatus.Outdated
					)
			};
		}

		internal static IEnumerable<SceneryMarkerModel> GenerateCustomSceneryMarkers(
			string customSceneryFolderPath
			)
		{
			if (!Directory.Exists(customSceneryFolderPath))
			{
				return new List<SceneryMarkerModel>(0);
			}

			var sceneryInfos = new List<SceneryMarkerModel>();

			var sceneryDirectories = Directory.GetDirectories(
				customSceneryFolderPath
				);

			for (var i = 0; i < sceneryDirectories.Length; i++)
			{
				OnMapSceneryProcess?.Invoke(
					sceneryDirectories.Length,
					i
					);

				var someCustomSceneryDirectory = sceneryDirectories[i];

				var aptDatFilePath = string.Concat(
					someCustomSceneryDirectory,
					@"\Earth nav data\apt.dat"
					);

				if (!File.Exists(aptDatFilePath))
				{
					continue;
				}

				using (var aptDatReader = new StreamReader(aptDatFilePath))
				{
					SceneryMarkerModel sceneryModel = null;

					while (aptDatReader.Peek() >= 0)
					{
						try
						{
							var someAptDatLine = aptDatReader.ReadLine();

							if (string.IsNullOrEmpty(someAptDatLine))
							{
								continue;
							}

							var lineParts = someAptDatLine.Split(' ');

							var lineIsAirportDescription = (
								(lineParts.Length > 0)
								&& Utils.StringsAreEqual("1", lineParts[0])
								);

							if (lineIsAirportDescription)
							{
								sceneryModel = new SceneryMarkerModel
								{
									SceneryFolder = Utils.NormalizeString(
										Path.GetFileName(
											someCustomSceneryDirectory
											)
										)
								};

								PopulateModelByNameData(
									someAptDatLine,
									sceneryModel
									);
							}

							var lineIsRunwayDescription = (
								(lineParts.Length > 0)
								&& Utils.StringsAreEqual("100", lineParts[0])
								);

							if (
								!lineIsRunwayDescription
								|| (null == sceneryModel)
								)
							{
								continue;
							}

							PopulateModelByLocationData(
								someAptDatLine,
								sceneryModel
								);
						}
						catch (Exception)
						{
							sceneryModel = null;
						}

						if (null == sceneryModel)
						{
							continue;
						}

						sceneryInfos.Add(sceneryModel);

						sceneryModel = null;
					}
				}
			}

			return sceneryInfos;
		}

		internal static void UpdateSceneryMarkerScriptFile(
			IEnumerable<SceneryMarkerModel> sceneryModels
			)
		{
			var markersJavaScriptBuilder = new StringBuilder();

			markersJavaScriptBuilder.AppendLine(
				@"function addSceneryMarkers(googleMapObject) {"
				);

			markersJavaScriptBuilder.AppendLine(
				@"
				if (null === googleMapObject) {
					return;
				}"
				);

			foreach (var sceneryInfo in sceneryModels)
			{
				markersJavaScriptBuilder.AppendFormat(
					@"
						new google.maps.Marker({{
							position: new google.maps.LatLng({0},{1}),
							map: googleMapObject,
							title: '{2}\n{3}\n{4}'
						}});
					"
					, sceneryInfo.Lat.ToString(CultureInfo.InvariantCulture)
					, sceneryInfo.Lon.ToString(CultureInfo.InvariantCulture)
					, sceneryInfo.IcaoCode
					, sceneryInfo.AirportName
					, sceneryInfo.SceneryFolder
					);
			}

			markersJavaScriptBuilder.AppendLine(
				@"}"
				);

			File.WriteAllText(
				GetSceneryMarkersFileFullPath(),
				markersJavaScriptBuilder.ToString()
				);
		}

		internal static void RemoveMapData()
		{
			File.Delete(
				GetSceneryMarkersFileFullPath()
				);
		}

		private static int GetColumnStringIndex(
			string aptString,
			int columnNumber
			)
		{
			if (string.IsNullOrEmpty(aptString))
			{
				return -1;
			}

			var previousCharIsSpace = false;
			var currentColumnIdx = 0;
			var retVal = 0;

			for (var i = 0; i < aptString.Length; i++)
			{
				var currentChar = aptString[i];
				if (
					previousCharIsSpace
					&& (' ' == currentChar)
					)
				{
					continue;
				}

				var currentCharIsSpace = (' ' == currentChar);
				if (currentCharIsSpace && !previousCharIsSpace)
				{
					currentColumnIdx++;
				}

				previousCharIsSpace = currentCharIsSpace;
				retVal = i;

				if (currentColumnIdx == columnNumber)
				{
					break;
				}
			}

			if (' ' != aptString[retVal])
			{
				return retVal;
			}

			for (var i = retVal + 1; i < aptString.Length; i++)
			{
				if (' ' == aptString[i])
				{
					continue;
				}

				retVal = i - 1;
				break;
			}

			return retVal;
		}

		private static void PopulateModelByNameData(
			string aptLine,
			SceneryMarkerModel sceneryModel
			)
		{
			var columnIdx = GetColumnStringIndex(
				aptLine,
				4
				);

			sceneryModel.IcaoCode = Utils.NormalizeString(
				aptLine.Substring(
					columnIdx + 1,
					4
				)
				);

			columnIdx = GetColumnStringIndex(
				aptLine,
				5
				);

			sceneryModel.AirportName = Utils.NormalizeString(
				aptLine.Substring(
					columnIdx + 1,
					aptLine.Length - columnIdx - 1
				)
				);
		}

		private static void PopulateModelByLocationData(
			string aptLine,
			SceneryMarkerModel sceneryModel
			)
		{
			var columnIdx = GetColumnStringIndex(
				aptLine,
				9
				);

			sceneryModel.Lat = double.Parse(
				Utils.ReadStringUntilSpace(aptLine, columnIdx),
				CultureInfo.InvariantCulture
				);

			columnIdx = GetColumnStringIndex(
				aptLine,
				10
				);

			sceneryModel.Lon = double.Parse(
				Utils.ReadStringUntilSpace(aptLine, columnIdx),
				CultureInfo.InvariantCulture
				);
		}
	}
}
