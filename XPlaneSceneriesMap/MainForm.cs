﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using XPlaneSceneriesMap.Configuration;
using XPlaneSceneriesMap.Core;
using XPlaneSceneriesMap.Core.Enums;
using XPlaneSceneriesMap.Models;
using XPlaneSceneriesMap.Properties;

namespace XPlaneSceneriesMap
{
	public partial class MainForm : Form
	{
		private readonly BackgroundWorker buildSceneryMarkersWorker;

		public MainForm()
		{
			InitializeComponent();

			buildSceneryMarkersWorker = new BackgroundWorker
			{
				WorkerReportsProgress = true,
				WorkerSupportsCancellation = false
			};

			buildSceneryMarkersWorker.DoWork += BuildSceneryMarkersWorker_DoWork;
			buildSceneryMarkersWorker.ProgressChanged += BuildSceneryMarkersWorker_ProgressChanged;
			buildSceneryMarkersWorker.RunWorkerCompleted += BuildSceneryMarkersWorker_RunWorkerCompleted;

			Worker.OnMapSceneryProcess += Worker_OnMapSceneryProcess;
		}

		private void MainForm_Load(
			object sender,
			EventArgs e
			)
		{
			LoadUiSettings();
		}

		private void btnBrowseForCustomSceneryFolder_Click(
			object sender,
			EventArgs e
			)
		{
			string selectedPath;

			using (
				var folderBrowserDialog = new FolderBrowserDialog
				{
					RootFolder = Environment.SpecialFolder.MyComputer,
					Description = Resources.BrowseForCustomSceneryFolderDialogDescription,
					ShowNewFolderButton = false
				}
				)
			{
				if (DialogResult.OK != folderBrowserDialog.ShowDialog())
				{
					return;
				}

				selectedPath = folderBrowserDialog.SelectedPath;
			}

			//	validate, basically, the selected folder
			//

			if (!Directory.Exists(selectedPath ?? string.Empty))
			{
				selectedPath = string.Empty;
			}

			txtCustomSceneryFolderPath.Text = selectedPath;
		}

		private void btnGenerateMap_Click(object sender, EventArgs e)
		{
			Configurator.SaveUiSettings(
				txtCustomSceneryFolderPath.Text
				);

			btnGenerateMap.Enabled = false;
			btnOpenMap.Enabled = false;
			btnChckMapStatus.Enabled = false;

			lblMapStatus.Text = Resources.MapStatusIsInProgress;

			buildSceneryMarkersWorker.RunWorkerAsync();
		}

		private void btnOpenMap_Click(object sender, EventArgs e)
		{
			var sceneryMapFullPath = Worker.GetSceneryMarkersPageFullPath();
			if (!File.Exists(sceneryMapFullPath))
			{
				InitializeMapStatusControls();

				return;
			}

			Process.Start(sceneryMapFullPath);
		}

		private void btnChckMapStatus_Click(
			object sender,
			EventArgs e
			)
		{
			Configurator.SaveUiSettings(
				txtCustomSceneryFolderPath.Text
				);

			InitializeMapStatusControls();
		}

		private void LoadUiSettings()
		{
			txtCustomSceneryFolderPath.Text = Configurator.GetCustomSceneryFolder();

			InitializeMapStatusControls();
		}

		private void InitializeMapStatusControls()
		{
			btnGenerateMap.Enabled = true;
			btnChckMapStatus.Enabled = true;

			var sceneryMapStatusModel = Worker.GetSceneryMapStatus(
				txtCustomSceneryFolderPath.Text
				);

			Color labelColor;
			string mapStatusText;

			switch (sceneryMapStatusModel.MapStatus)
			{
				case SceneriesMapStatus.Actual:
					{
						labelColor = Color.Green;

						mapStatusText = Resources.MapStatusActual;

						btnGenerateMap.Text = Resources.BtnGenerateMapTextUpdate;
						btnOpenMap.Enabled = true;
					}
					break;

				case SceneriesMapStatus.NotAvailable:
					{
						labelColor = Color.Red;

						mapStatusText = Resources.MapStatusNotAvailable;

						btnGenerateMap.Text = Resources.BtnGenerateMapTextGenerate;
						btnOpenMap.Enabled = false;
					}
					break;

				case SceneriesMapStatus.Outdated:
					{
						labelColor = Color.Blue;

						mapStatusText = string.Format(
							Resources.MapStatusOutdated,
							sceneryMapStatusModel.CustomSceneryFolderLastEditTimestamp,
							sceneryMapStatusModel.MarkersFileLastEditTimestamp
							);

						btnGenerateMap.Text = Resources.BtnGenerateMapTextUpdate;
						btnOpenMap.Enabled = true;
					}
					break;

				default:
					{
						labelColor = lblMapStatus.ForeColor;
						mapStatusText = string.Empty;
					}
					break;
			}

			lblMapStatus.ForeColor = labelColor;
			lblMapStatus.Text = mapStatusText;

			lblStatusCheckedAt.Text = sceneryMapStatusModel.StatusCheckedAt.ToString(
				CultureInfo.CurrentUICulture
				);
		}


		#region buildSceneryMarkersWorker event handlers

		private void BuildSceneryMarkersWorker_DoWork(
			object sender,
			DoWorkEventArgs e
			)
		{
			e.Result = new List<SceneryMarkerModel>(
				Worker.GenerateCustomSceneryMarkers(
					txtCustomSceneryFolderPath.Text
				)
			);
		}

		private void BuildSceneryMarkersWorker_RunWorkerCompleted(
			object sender,
			RunWorkerCompletedEventArgs e
			)
		{
			var generatedMarkers = e.Result as List<SceneryMarkerModel>;

			var weHaveGeneratedMarkers = (
				(null != generatedMarkers)
				&& (generatedMarkers.Count > 0)
				);

			if (weHaveGeneratedMarkers)
			{
				Worker.UpdateSceneryMarkerScriptFile(
					generatedMarkers
					);

				btnOpenMap_Click(null, null);
			}
			else
			{
				//	we have no any markers, remove previous map data
				//

				Worker.RemoveMapData();
			}

			Configurator.UpdateCustomFolderHashValue(
				txtCustomSceneryFolderPath.Text
				);

			InitializeMapStatusControls();
		}

		private void BuildSceneryMarkersWorker_ProgressChanged(
			object sender,
			ProgressChangedEventArgs e
			)
		{
			lblMapStatus.Text = string.Format(
				Resources.MapStatusProgress,
				e.ProgressPercentage
				);
		}

		#endregion


		private void Worker_OnMapSceneryProcess(
			int numberOfSceneries,
			int currentSceneryIdx
			)
		{
			var progressPercentage = (
				Convert.ToDouble(currentSceneryIdx) / Convert.ToDouble(numberOfSceneries)
				);

			buildSceneryMarkersWorker.ReportProgress(
				Convert.ToInt32(progressPercentage * 100)
				);
		}

		private void txtCustomSceneryFolderPath_TextChanged(
			object sender,
			EventArgs e
			)
		{
			lblMapStatus.Text = Resources.MapStatusUnknown;

			btnGenerateMap.Enabled = true;
			btnGenerateMap.Text = Resources.BtnGenerateMapTextGenerate;

			btnOpenMap.Enabled = false;
			btnChckMapStatus.Enabled = false;
		}
	}
}
