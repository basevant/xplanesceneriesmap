﻿namespace XPlaneSceneriesMap
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.GroupBox grpXPlaneFolder;
			System.Windows.Forms.GroupBox grpSceneriesMap;
			System.Windows.Forms.Label lblStatusCheckedAtLabel;
			System.Windows.Forms.Label lblMapStatusLabel;
			this.btnBrowseForCustomSceneryFolder = new System.Windows.Forms.Button();
			this.txtCustomSceneryFolderPath = new System.Windows.Forms.TextBox();
			this.lblStatusCheckedAt = new System.Windows.Forms.Label();
			this.btnChckMapStatus = new System.Windows.Forms.Button();
			this.btnOpenMap = new System.Windows.Forms.Button();
			this.btnGenerateMap = new System.Windows.Forms.Button();
			this.lblMapStatus = new System.Windows.Forms.Label();
			grpXPlaneFolder = new System.Windows.Forms.GroupBox();
			grpSceneriesMap = new System.Windows.Forms.GroupBox();
			lblStatusCheckedAtLabel = new System.Windows.Forms.Label();
			lblMapStatusLabel = new System.Windows.Forms.Label();
			grpXPlaneFolder.SuspendLayout();
			grpSceneriesMap.SuspendLayout();
			this.SuspendLayout();
			// 
			// grpXPlaneFolder
			// 
			grpXPlaneFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			grpXPlaneFolder.Controls.Add(this.btnBrowseForCustomSceneryFolder);
			grpXPlaneFolder.Controls.Add(this.txtCustomSceneryFolderPath);
			grpXPlaneFolder.Location = new System.Drawing.Point(8, 16);
			grpXPlaneFolder.Name = "grpXPlaneFolder";
			grpXPlaneFolder.Size = new System.Drawing.Size(568, 56);
			grpXPlaneFolder.TabIndex = 3;
			grpXPlaneFolder.TabStop = false;
			grpXPlaneFolder.Text = "Папка \"Custom Scenery\"";
			// 
			// btnBrowseForCustomSceneryFolder
			// 
			this.btnBrowseForCustomSceneryFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnBrowseForCustomSceneryFolder.Location = new System.Drawing.Point(465, 22);
			this.btnBrowseForCustomSceneryFolder.Name = "btnBrowseForCustomSceneryFolder";
			this.btnBrowseForCustomSceneryFolder.Size = new System.Drawing.Size(95, 25);
			this.btnBrowseForCustomSceneryFolder.TabIndex = 4;
			this.btnBrowseForCustomSceneryFolder.Text = "Указать...";
			this.btnBrowseForCustomSceneryFolder.UseVisualStyleBackColor = true;
			this.btnBrowseForCustomSceneryFolder.Click += new System.EventHandler(this.btnBrowseForCustomSceneryFolder_Click);
			// 
			// txtCustomSceneryFolderPath
			// 
			this.txtCustomSceneryFolderPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtCustomSceneryFolderPath.Location = new System.Drawing.Point(9, 24);
			this.txtCustomSceneryFolderPath.Name = "txtCustomSceneryFolderPath";
			this.txtCustomSceneryFolderPath.Size = new System.Drawing.Size(448, 20);
			this.txtCustomSceneryFolderPath.TabIndex = 3;
			this.txtCustomSceneryFolderPath.TextChanged += new System.EventHandler(this.txtCustomSceneryFolderPath_TextChanged);
			// 
			// grpSceneriesMap
			// 
			grpSceneriesMap.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			grpSceneriesMap.Controls.Add(this.lblStatusCheckedAt);
			grpSceneriesMap.Controls.Add(lblStatusCheckedAtLabel);
			grpSceneriesMap.Controls.Add(this.btnChckMapStatus);
			grpSceneriesMap.Controls.Add(this.btnOpenMap);
			grpSceneriesMap.Controls.Add(this.btnGenerateMap);
			grpSceneriesMap.Controls.Add(this.lblMapStatus);
			grpSceneriesMap.Controls.Add(lblMapStatusLabel);
			grpSceneriesMap.Location = new System.Drawing.Point(8, 88);
			grpSceneriesMap.Name = "grpSceneriesMap";
			grpSceneriesMap.Size = new System.Drawing.Size(568, 105);
			grpSceneriesMap.TabIndex = 4;
			grpSceneriesMap.TabStop = false;
			grpSceneriesMap.Text = "Карта сценариев";
			// 
			// lblStatusCheckedAt
			// 
			this.lblStatusCheckedAt.AutoSize = true;
			this.lblStatusCheckedAt.Location = new System.Drawing.Point(88, 27);
			this.lblStatusCheckedAt.Name = "lblStatusCheckedAt";
			this.lblStatusCheckedAt.Size = new System.Drawing.Size(35, 13);
			this.lblStatusCheckedAt.TabIndex = 6;
			this.lblStatusCheckedAt.Text = "label1";
			// 
			// lblStatusCheckedAtLabel
			// 
			lblStatusCheckedAtLabel.AutoSize = true;
			lblStatusCheckedAtLabel.Location = new System.Drawing.Point(8, 27);
			lblStatusCheckedAtLabel.Name = "lblStatusCheckedAtLabel";
			lblStatusCheckedAtLabel.Size = new System.Drawing.Size(66, 13);
			lblStatusCheckedAtLabel.TabIndex = 5;
			lblStatusCheckedAtLabel.Text = "Проверено:";
			// 
			// btnChckMapStatus
			// 
			this.btnChckMapStatus.Location = new System.Drawing.Point(392, 72);
			this.btnChckMapStatus.Name = "btnChckMapStatus";
			this.btnChckMapStatus.Size = new System.Drawing.Size(168, 25);
			this.btnChckMapStatus.TabIndex = 4;
			this.btnChckMapStatus.Text = "Проверить статус";
			this.btnChckMapStatus.UseVisualStyleBackColor = true;
			this.btnChckMapStatus.Click += new System.EventHandler(this.btnChckMapStatus_Click);
			// 
			// btnOpenMap
			// 
			this.btnOpenMap.Location = new System.Drawing.Point(200, 72);
			this.btnOpenMap.Name = "btnOpenMap";
			this.btnOpenMap.Size = new System.Drawing.Size(168, 25);
			this.btnOpenMap.TabIndex = 3;
			this.btnOpenMap.Text = "Показать...";
			this.btnOpenMap.UseVisualStyleBackColor = true;
			this.btnOpenMap.Click += new System.EventHandler(this.btnOpenMap_Click);
			// 
			// btnGenerateMap
			// 
			this.btnGenerateMap.Location = new System.Drawing.Point(8, 72);
			this.btnGenerateMap.Name = "btnGenerateMap";
			this.btnGenerateMap.Size = new System.Drawing.Size(168, 25);
			this.btnGenerateMap.TabIndex = 2;
			this.btnGenerateMap.Text = "Создать...";
			this.btnGenerateMap.UseVisualStyleBackColor = true;
			this.btnGenerateMap.Click += new System.EventHandler(this.btnGenerateMap_Click);
			// 
			// lblMapStatus
			// 
			this.lblMapStatus.AutoSize = true;
			this.lblMapStatus.Location = new System.Drawing.Point(88, 48);
			this.lblMapStatus.Name = "lblMapStatus";
			this.lblMapStatus.Size = new System.Drawing.Size(68, 13);
			this.lblMapStatus.TabIndex = 1;
			this.lblMapStatus.Text = "lblMapStatus";
			// 
			// lblMapStatusLabel
			// 
			lblMapStatusLabel.AutoSize = true;
			lblMapStatusLabel.Location = new System.Drawing.Point(8, 48);
			lblMapStatusLabel.Name = "lblMapStatusLabel";
			lblMapStatusLabel.Size = new System.Drawing.Size(78, 13);
			lblMapStatusLabel.TabIndex = 0;
			lblMapStatusLabel.Text = "Статус карты:";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(649, 214);
			this.Controls.Add(grpSceneriesMap);
			this.Controls.Add(grpXPlaneFolder);
			this.MaximizeBox = false;
			this.MinimumSize = new System.Drawing.Size(665, 253);
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "XPlaneSceneriesMap";
			this.Load += new System.EventHandler(this.MainForm_Load);
			grpXPlaneFolder.ResumeLayout(false);
			grpXPlaneFolder.PerformLayout();
			grpSceneriesMap.ResumeLayout(false);
			grpSceneriesMap.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnBrowseForCustomSceneryFolder;
		private System.Windows.Forms.TextBox txtCustomSceneryFolderPath;
		private System.Windows.Forms.Label lblMapStatus;
		private System.Windows.Forms.Button btnGenerateMap;
		private System.Windows.Forms.Button btnOpenMap;
		private System.Windows.Forms.Button btnChckMapStatus;
		private System.Windows.Forms.Label lblStatusCheckedAt;
	}
}

