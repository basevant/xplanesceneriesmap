﻿using System;
using System.Configuration;
using System.IO;
using XPlaneSceneriesMap.Core;

namespace XPlaneSceneriesMap.Configuration
{
	internal static class Configurator
	{
		private const string CustomSceneryFolderParamName
			= @"CustomSceneryFolder";

		internal static string CustomSceneryFolderHash
			=> ReadStringValue(nameof(CustomSceneryFolderHash));

		internal static string GetCustomSceneryFolder()
		{
			var configuredPath = ReadStringValue(
				CustomSceneryFolderParamName
				);

			if (Directory.Exists(configuredPath))
			{
				return configuredPath;
			}

			//	try to find X-Plane instances
			//

			//	check the installed '< X-Plane 10' instances
			//

			var retVal = FindFirstCustomSceneryFolderInstance(
				BuildXPlaneInstancesFilePath(true)
				);

			if (Directory.Exists(retVal))
			{
				return retVal;
			}

			//	check the installed '>= X-Plane 10' instances
			//

			retVal = FindFirstCustomSceneryFolderInstance(
				BuildXPlaneInstancesFilePath(false)
				);

			return (
				Directory.Exists(retVal)
				? retVal
				: string.Empty
				);
		}

		internal static bool SaveUiSettings(
			string customSceneryPath
			)
			=> UpdateConfigSetting(
				CustomSceneryFolderParamName,
				customSceneryPath
				);

		internal static bool UpdateCustomFolderHashValue(
			string customSceneryFolderLocation
			)
			=> UpdateConfigSetting(
					nameof(CustomSceneryFolderHash),
					Utils.CalculateTopFoldersHash(
						customSceneryFolderLocation
					)
				);

		private static string ReadStringValue(string paramName)
		{
			string retVal;

			try
			{
				retVal = ConfigurationManager.AppSettings[paramName];
			}
			catch (Exception)
			{
				retVal = string.Empty;
			}

			return retVal;
		}

		private static string BuildXPlaneInstancesFilePath(
			bool buildPrior10VersionInstancesPath
			)
		{
			var xplaneInstancesFilename = (
				buildPrior10VersionInstancesPath
				? "x-plane_install.txt"
				: "x-plane_install_10.txt"
				);

			return Path.Combine(
				Environment.GetFolderPath(
					Environment.SpecialFolder.LocalApplicationData
					),
				xplaneInstancesFilename
				);
		}

		private static string FindFirstCustomSceneryFolderInstance(
			string xplaneInstancesFilePath
			)
		{
			if (!File.Exists(xplaneInstancesFilePath))
			{
				return string.Empty;
			}

			string retVal = null;

			try
			{
				var installedInstances = File.ReadAllLines(
					xplaneInstancesFilePath
					);

				foreach (var someInstance in installedInstances)
				{
					var customSceneryPath = Path.Combine(
						someInstance,
						"Custom Scenery"
						).Replace('/', '\\');

					if (!Directory.Exists(customSceneryPath))
					{
						continue;
					}

					retVal = customSceneryPath;

					break;
				}
			}
			catch (Exception)
			{
				retVal = null;
			}

			return retVal ?? string.Empty;
		}

		private static bool UpdateConfigSetting(
			string settingName,
			string value
			)
		{
			bool retVal;

			try
			{
				var exeConfiguration = ConfigurationManager.OpenExeConfiguration(
					ConfigurationUserLevel.None
					);

				exeConfiguration.AppSettings.Settings[settingName].Value
					= value;

				//	save and update application settings
				//

				exeConfiguration.Save(
					ConfigurationSaveMode.Modified
					);

				ConfigurationManager.RefreshSection(
					"appSettings"
					);

				retVal = true;
			}
			catch (Exception)
			{
				retVal = false;
			}

			return retVal;
		}
	}
}
