﻿using System;
using System.Windows.Forms;

namespace XPlaneSceneriesMap
{
	internal static class Starter
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		private static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			using (var mainForm = new MainForm())
			{
				Application.Run(mainForm);
			}
		}
	}
}
