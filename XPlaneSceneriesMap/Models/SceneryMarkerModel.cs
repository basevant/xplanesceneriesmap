﻿namespace XPlaneSceneriesMap.Models
{
	internal class SceneryMarkerModel
	{
		internal string IcaoCode { get; set; }

		internal string AirportName { get; set; }

		internal string SceneryFolder { get; set; }

		public double Lat { get; set; }

		public double Lon { get; set; }
	}
}
