﻿using System;
using XPlaneSceneriesMap.Core.Enums;

namespace XPlaneSceneriesMap.Models
{
	internal class SceneryMapStatusModel
	{
		internal SceneriesMapStatus MapStatus { get; set; }

		internal DateTime? MarkersFileLastEditTimestamp { get; set; }

		internal DateTime? CustomSceneryFolderLastEditTimestamp { get; set; }

		internal DateTime StatusCheckedAt { get; }

		internal SceneryMapStatusModel()
		{
			StatusCheckedAt = DateTime.Now;
		}
	}
}
